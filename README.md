# Resteam

Your game store and launcher for all games that you want.

## Installation

You need to have this two main global dependencies installed:

- **Node >=20v** (if you have **nvm**, run `nvm install` or `nvm use`)
- **Docker**/Docker Engine and **Docker Compose** (both latest)
- **pnpm** as a package manager

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/programming3094413/semester-4/resteam.git
   ```

2. Install dependencies running `pnpm install`

3. Up containers with `pnpm services:up`

4. Run migrations with `pnpm migrations:run`

5. Seed database with `pnpm migrations:seed`

## Usage

1. Start the application running `pnpm start # or dev for development mode`
2. The API will be accessible at <http://localhost:3000>
3. All endpoints are in `api.http` file. You must have the extension **REST Client** - code: humao.rest-client

## Scripts

Below are the available scripts in the `package.json` and their descriptions:

| Command            | Description                                                                 |
|--------------------|-----------------------------------------------------------------------------|
| `dev`              | Starts the server in development mode with automatic restart (`watch`).     |
| `start`            | Starts the server in production mode.                                       |
| `lint`             | Checks and automatically fixes the source files (`src`) using Biome.        |
| `migration:run`    | Runs the database migrations.                                               |
| `migration:seed`   | Seed the database with some fake data using faker-js.                       |
| `docker:compose`   | Executes the `docker-compose` command with the `docker-compose.yaml` file.  |
| `services:up`      | Starts the services defined in `docker-compose.yaml` in detached mode.      |
| `services:stop`    | Stops the services defined in `docker-compose.yaml`.                        |
| `services:down`    | Brings down the services defined in `docker-compose.yaml`.                  |
