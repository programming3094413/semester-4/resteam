import { ZodError } from "zod";

export function errorHandler(err, req, res, next) {
  if (err instanceof ZodError) {
    return res.status(400).json({
      name: err.name,
      status: 400,
      message: JSON.parse(err.message)
    });
  }

  const status = err.status || 500;

  res.status(status).json({
    name: err.name,
    status,
    message: err.message
  });
}
