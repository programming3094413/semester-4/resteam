export function logger(req, res, next) {
  const date = new Date();
  const now = date.toISOString();
  console.log(`${now} - ${req.method} ${req.url}`);
  next();
}
