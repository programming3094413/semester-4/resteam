import postgres from "postgres";

export const pg = postgres({
  host: "localhost",
  port: 5432,
  database: "resteam",
  username: "admin",
  password: "supersecret"
});
