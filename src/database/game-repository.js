import { pg } from "./postgres.js";

export const gameRepository = {
  async getAll() {
    return pg /*sql*/`
      SELECT * FROM "tb_games"
    `;
  },
  async getById(id) {
    const [game] = await pg /*sql*/`
      SELECT * FROM "tb_games" WHERE id::text = ${id};
    `;

    return game;
  },
  async create(id, name, description, genre, platform) {
    const [newGame] = await pg /*sql*/`
      INSERT INTO "tb_games"
      VALUES (${id}, ${name}, ${description}, ${genre}, ${platform})
      RETURNING *
    `;

    return newGame;
  },
  async update(id, fields) {
    const columns = Object.keys(fields);

    return pg /*sql*/`
      UPDATE "tb_games"
      SET ${pg(fields, columns)}
      WHERE id::text = ${id}
      RETURNING *
    `;
  },
  async delete(id) {
    await pg /*sql*/`DELETE FROM "tb_games" WHERE id::text = ${id}`;
  }
};
