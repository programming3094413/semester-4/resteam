import { migrate } from "postgres-migrations";
import { pg } from "./postgres.js";

import path from "node:path";

const config = {
  database: "resteam",
  user: "admin",
  password: "supersecret",
  host: "localhost",
  port: 5432,
  ensureDatabaseExists: true,
  defaultDatabase: "resteam"
};

try {
  await migrate(config, path.join(process.cwd(), "migrations"));
  console.log("Migrations done!");
} catch (e) {
  if (e instanceof Error) {
    console.error(`ERROR: ${e.message}`);
  } else {
    console.log("An error occur.");
  }
} finally {
  pg.end();
}
