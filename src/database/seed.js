import { faker } from "@faker-js/faker";
import { gameRepository } from "./game-repository.js";
import { pg } from "./postgres.js";

export const games = Array.from({ length: 5 }).map(() => {
  return {
    id: faker.string.nanoid(10),
    name: faker.word.sample(),
    description: faker.lorem.words(5),
    genre: faker.internet.protocol(),
    type: "DESKTOP"
  };
});

try {
  if ((await gameRepository.getAll()).length > 0) {
    console.log("Database already has data!");
    pg.end();
    process.exit(0);
  }

  for (const game of games) {
    const { id, name, description, genre, type } = game;

    await gameRepository.create(id, name, description, genre, type);
  }

  console.log("Database seeded!");
} catch (error) {
  console.error("Error seeding database:", error);
} finally {
  pg.end();
}
