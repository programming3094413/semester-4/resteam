import express from "express";
import { errorHandler } from "./middlewares/error-handler.js";
import { logger } from "./middlewares/logger.js";
import gameRoutes from "./routes.js";

const app = express();
const port = process.env.PORT || 3333;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(logger);

app.use("/api", gameRoutes);

app.use(errorHandler);

app.listen(port, () => {
  console.log(`API listening on port: ${port}`);
});
