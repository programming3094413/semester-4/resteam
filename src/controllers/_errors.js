export class ApplicationError extends Error {
  constructor(name, message, status) {
    super(message);
    Error.captureStackTrace(this, ApplicationError);

    this.name = name || "BadRequest";
    this.message = message || "Something went wrong.";
    this.status = status || 500;
  }
}

export class GameNotFound extends ApplicationError {
  constructor() {
    super("GameNotFoundError", "Game not found", 404);
  }
}
