import { gameRepository } from "../database/game-repository.js";

export async function getAllGames(_, res, next) {
  try {
    const games = await gameRepository.getAll();

    res.json(games);
  } catch (err) {
    return next(err);
  }
}
