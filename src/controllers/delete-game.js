import { z } from "zod";
import { gameRepository } from "../database/game-repository.js";
import { GameNotFound } from "./_errors.js";

const paramsSchema = z.object({
  id: z.string()
});

export async function deleteGame(req, res, next) {
  try {
    const { id } = paramsSchema.parse(req.params);
    const game = await gameRepository.getById(id);

    if (!game) {
      throw new GameNotFound();
    }

    await gameRepository.delete(id);
    res.sendStatus(204);
  } catch (err) {
    return next(err);
  }
}
