import { z } from "zod";
import { gameRepository } from "../database/game-repository.js";
import { GameNotFound } from "./_errors.js";

const paramsSchema = z.object({
  id: z.string()
});

export async function getGameById(req, res, next) {
  try {
    const { id } = paramsSchema.parse(req.params);
    if (!(await gameRepository.getById(id))) {
      throw new GameNotFound();
    }

    const game = await gameRepository.getById(id);
    res.json(game);
  } catch (err) {
    return next(err);
  }
}
