import { nanoid } from "nanoid";
import { z } from "zod";
import { gameRepository } from "../database/game-repository.js";

const bodySchema = z.object({
  name: z.string(),
  description: z.string(),
  genre: z.string(),
  platform: z.enum(["DESKTOP", "MOBILE", "CONSOLE"])
});

export async function createGame(req, res, next) {
  try {
    const { name, description, genre, platform } = bodySchema.parse(req.body);

    const id = nanoid(10);

    const newGame = await gameRepository.create(
      id,
      name,
      description,
      genre,
      platform
    );

    res.status(201).json(newGame);
  } catch (err) {
    return next(err);
  }
}
