import { z } from "zod";
import { gameRepository } from "../database/game-repository.js";
import { GameNotFound } from "./_errors.js";

const paramsSchema = z.object({
  id: z.string()
});

const bodySchema = z.object({
  name: z.string().optional(),
  description: z.string().optional(),
  genre: z.string().optional(),
  platform: z.enum(["DESKTOP", "MOBILE", "CONSOLE"]).optional()
});

export async function updateGame(req, res, next) {
  try {
    const { id } = paramsSchema.parse(req.params);
    const data = bodySchema.parse(req.body);
    if (!(await gameRepository.getById(id))) {
      throw new GameNotFound();
    }

    const gameUpdated = await gameRepository.update(id, data);
    res.json(gameUpdated);
  } catch (err) {
    return next(err);
  }
}
