import express from "express";
import { createGame } from "./controllers/create-game.js";
import { deleteGame } from "./controllers/delete-game.js";
import { getAllGames } from "./controllers/get-all-games.js";
import { getGameById } from "./controllers/get-game-by-id.js";
import { updateGame } from "./controllers/update-game.js";

const router = express.Router();

router.post("/games", createGame);
router.get("/games", getAllGames);
router.get("/games/:id", getGameById);
router.put("/games/:id", updateGame);
router.patch("/games/:id", updateGame);
router.delete("/games/:id", deleteGame);

export default router;
