import { nanoid } from "nanoid";

export class Game {
  constructor(name, description, genre, platform) {
    this.id = nanoid(10);
    this.name = name;
    this.description = description;
    this.genre = genre;
    this.platform = platform;
  }
}
