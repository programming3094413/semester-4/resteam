CREATE TYPE "game_platform" AS ENUM ('DESKTOP', 'MOBILE', 'CONSOLE');

ALTER TABLE "tb_games" ADD COLUMN platform "game_platform";